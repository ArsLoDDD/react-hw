import { useEffect } from 'react'
import './App.css'
import Header from './components/Header'
import AppRoutes from './AppRoutes'
import { useDispatch } from 'react-redux'
import { fetchItems } from './redux/actionCreators/itemsActionCreators'
import { setFavItems } from './redux/actionCreators/favoriteItemsActionCreators'
import { fetchAndSetCartItems } from './redux/actionCreators/cartItemsActionCreators'
import { useSelector } from 'react-redux'
import Modal from './components/Modal'
import { closeModal } from './redux/actionCreators/modalActionCreators'
import { ItemsContainerProvider } from './contexts/ItemsContainerContext'

function App() {
	const dispatch = useDispatch()
	const isModalOpen = useSelector(state => state.modal.isModalOpen)
	const { header, text, buttons } = useSelector(
		state => state.modal.modalContent
	)

	useEffect(() => {
		dispatch(fetchItems())
	}, [])

	useEffect(() => {
		if (localStorage.getItem('cartItems')) {
			dispatch(fetchAndSetCartItems())
		}
		if (localStorage.getItem('favoriteItems')) {
			dispatch(setFavItems(JSON.parse(localStorage.getItem('favoriteItems'))))
		}
	}, [])

	return (
		<ItemsContainerProvider>
			<div className='App'>
				{isModalOpen && (
					<Modal
						isCloseButton={isModalOpen}
						header={header}
						text={text}
						onClick={() => dispatch(closeModal())}
					>
						{buttons.map(button => {
							return button
						})}
					</Modal>
				)}
				<Header />

				<AppRoutes />
			</div>
		</ItemsContainerProvider>
	)
}

export default App
