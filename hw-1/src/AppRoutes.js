import React from 'react'
import { Routes, Route } from 'react-router-dom'
import CartPage from './pages/CartPage'
import ProductContainer from './components/ProductContainer'
import FavoritePage from './pages/FavoritePage'
import ItemPage from './pages/ItemPage'
import { useSelector } from 'react-redux'
import { shallowEqual } from 'react-redux'

const AppRoutes = () => {
	const cartItems = useSelector(
		state => state.cartItems.cartItems,
		shallowEqual
	)
	const favoriteItems = useSelector(
		state => state.favItems.favItems,
		shallowEqual
	)
	const cartPrice = useSelector(state => state.cartPrice.cartPrice)
	return (
		<Routes>
			<Route path='/' element={<ProductContainer />} />
			<Route
				path='/cart'
				element={<CartPage cartPrice={cartPrice} items={cartItems} />}
			/>
			<Route path='/cart/:id' element={<ItemPage items={cartItems} />} />
			<Route path='/favorite' element={<FavoritePage />} />
			<Route
				path='/favorite/:id'
				element={<ItemPage items={favoriteItems} />}
			/>
		</Routes>
	)
}

export default AppRoutes
