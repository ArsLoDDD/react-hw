import Button from '.'
import { render, screen } from '@testing-library/react'

describe('Smoke test (snapshot tests)', () => {
	test('should btn render', () => {
		const { asFragment } = render(
			<Button text='test test' className='test btn' backgroundColor='green' />
		)
		expect(asFragment()).toMatchSnapshot()
	})
})

describe('Button onClick tests', () => {
	test('should btn onClick can be clicked', () => {
		const mockOnClick = jest.fn()
		render(
			<Button
				text='test test'
				className='test btn'
				backgroundColor='green'
				onClick={mockOnClick}
			/>
		)
		const btn = screen.getByText('test test')
		btn.click()
		expect(mockOnClick).toBeCalled()
	})
})
