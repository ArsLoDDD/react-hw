import React from 'react'
import styles from './Button.module.scss'
import propTypes from 'prop-types'

const Button = ({ backgroundColor, text, onClick }) => {
	return (
		<button
			onClick={onClick}
			className={styles.button}
			style={{ backgroundColor }}
		>
			{text}
		</button>
	)
}

Button.propTypes = {
	backgroundColor: propTypes.string,
	text: propTypes.string,
	onClick: propTypes.func,
}

Button.defaultProps = {
	backgroundColor: 'black',
	text: 'Button',
	onClick: () => {},
}
export default Button
