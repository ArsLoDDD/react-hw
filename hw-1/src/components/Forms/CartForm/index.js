import React from 'react'
import { Formik, Form } from 'formik'
import Input from '../../Input'
import validationSchema from './validationSchema.js'
import { useDispatch } from 'react-redux'
import { resetCartItems } from '../../../redux/actionCreators/cartItemsActionCreators'
import { setCartCount } from '../../../redux/actionCreators/cartCountActionCreators'
import { setCartPrice } from '../../../redux/actionCreators/cartPriceActionCreators'
import styles from './CartForm.module.scss'

const Forms = ({ currentItem, cartPrice }) => {
	const dispatch = useDispatch()
	const initialValues = {
		name: '',
		lastName: '',
		age: '',
		address: '',
		phone: '',
	}

	const onSubmit = (values, action) => {
		console.log({ ...values, order: currentItem, oredPrice: cartPrice })
		dispatch(resetCartItems())
		dispatch(setCartCount(0))
		dispatch(setCartPrice(0))
	}

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={onSubmit}
		>
			{form => (
				<Form className={styles.root}>
					<Input name='name' />
					<Input name='lastName' />
					<Input name='age' />
					<Input name='address' />
					<Input phone={true} name='phone' />
					<button className={styles.btn} disabled={!form.isValid} type='submit'>
						Checkout
					</button>
				</Form>
			)}
		</Formik>
	)
}

export default Forms
