import * as Yup from 'yup'

const validationSchema = Yup.object().shape({
	name: Yup.string()
		.required('Name is required')
		.min(3, 'Name must be at least 3 characters'),
	lastName: Yup.string()
		.required('Last name is required')
		.min(3, 'Last name must be at least 3 characters'),
	age: Yup.number()
		.required('Age is required')
		.min(18, 'You must be at least 18 years old'),
	address: Yup.string()
		.required('Address is required')
		.min(3, 'Address must be at least 3 characters'),
	phone: Yup.string()
		.required('Phone is required')
		.min(10, 'Phone number must be 10 characters'),
})

export default validationSchema
