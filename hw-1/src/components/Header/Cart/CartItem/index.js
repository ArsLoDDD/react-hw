import React from 'react'
import styles from './CartItem.module.scss'
import propTypes from 'prop-types'

const CartItem = ({ name, price, image, count }) => {
	return (
		<div className={styles.cartItem}>
			<img src={image} alt='' />
			<p className={styles.cartItemName}>{name}</p>
			<p>{count}</p>
			<p className={styles.cartItemPrice}>{price * count}</p>
		</div>
	)
}

CartItem.propTypes = {
	name: propTypes.string,
	price: propTypes.number,
	image: propTypes.string,
	count: propTypes.number,
}
CartItem.defaultProps = {
	name: '',
	price: 0,
	image: '',
	count: 1,
}
export default CartItem
