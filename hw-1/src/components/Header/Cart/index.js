import React, { useEffect } from 'react'
import Icon from '@mdi/react'
import { mdiCart, mdiStar } from '@mdi/js'
import styles from './Cart.module.scss'
import propTypes from 'prop-types'
import { useSelector } from 'react-redux'
import { useState } from 'react'
import { shallowEqual } from 'react-redux'

const Cart = ({ handleIsCartOpen }) => {
	const favItems = useSelector(state => state.favItems.favItems, shallowEqual)

	const cartPrice = useSelector(state => state.cartPrice.cartPrice)
	const cartCount = useSelector(state => state.cartCount.cartCount)

	const [favItemsCount, setFavItemsCount] = useState(0)

	useEffect(() => {
		setFavItemsCount(() => {
			const array = favItems.filter(item => {
				return item.nowIsInFavorite === true
			})
			return array.length
		})
	}, [favItems])

	return (
		<div className={styles.headerIconcContainer}>
			<div className={styles.cartContainer}>
				<Icon
					onClick={() => handleIsCartOpen()}
					path={mdiCart}
					size={1}
					color='white'
				/>
				<div className={styles.cartCount}>
					<p>
						{cartCount} | {cartPrice}
					</p>
				</div>
			</div>
			<div className={styles.favoriteContainer}>
				<Icon path={mdiStar} size={1} color='white' />
				<div className={styles.favoriteCount}>
					<p>{favItemsCount}</p>
				</div>
			</div>
		</div>
	)
}
Cart.propTypes = {
	handleIsCartOpen: propTypes.func.isRequired,
}

export default Cart
