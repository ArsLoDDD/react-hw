import React from 'react'
import Cart from './Cart'
import styles from './Header.module.scss'
import CartItem from './Cart/CartItem'
import { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { shallowEqual } from 'react-redux'


const Header = () => {
	const [isCartOpen, setIsCartOpen] = useState(false)

	const cartItems = useSelector(state => state.cartItems.cartItems, shallowEqual)

	const handleIsCartOpen = () => {
		setIsCartOpen(prev => !prev)
	}

	return (
		<header className={styles.header}>
			<div className={styles.headerContainer}>
				<h1>Shop</h1>
				<nav>
					<ul className={styles.navList}>
						<li>
							<NavLink to='/'>Home</NavLink>
						</li>
						<li>
							<NavLink to='/favorite'>Favorite</NavLink>
						</li>
						<li>
							<NavLink to='/cart'>Cart</NavLink>
						</li>
					</ul>
				</nav>
				<Cart
					handleIsCartOpen={handleIsCartOpen}
				/>
			</div>
			{isCartOpen && (
				<div className={styles.cartItemsContainer}>
					{cartItems.map(item => (
						<CartItem
							key={item.sku}
							name={item.name}
							price={item.price}
							image={item.imageURL}
							count={item.count}
						/>
					))}
					{isCartOpen && cartItems.length === 0 && (
						<p className={styles.emptyCart}>Cart is empty</p>
					)}
				</div>
			)}
		</header>
	)
}

export default Header
