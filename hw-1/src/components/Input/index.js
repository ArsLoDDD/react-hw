import React from 'react'
import { useField } from 'formik'
import styles from './Input.module.scss'
import { PatternFormat } from 'react-number-format'

const Input = props => {
	const [field, meta] = useField(props)
	const { error, touched } = meta
	return (
		<div className={styles.root}>
			<label htmlFor={field.name}>
				<p className={styles.inputName}>
					{props.name.charAt(0).toUpperCase() + props.name.slice(1)}
				</p>

				{props.phone ? (
					<PatternFormat
						format='+38 (###) ###-##-##'
						mask='_'
						className={styles.input}
						id={field.name}
						{...field}
						{...props}
					/>
				) : (
					<input
						id={field.name}
						className={styles.input}
						{...field}
						{...props}
					/>
				)}
				{error && touched && <div className={styles.error}>{error}</div>}
			</label>
		</div>
	)
}

export default Input
