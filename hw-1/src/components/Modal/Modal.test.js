import Modal from '.'
import { render, screen, fireEvent } from '@testing-library/react'
import Button from '../Button'
import { useEffect, useState } from 'react'
import { Provider, useSelector } from 'react-redux'
import store from '../../redux/store'
import { useDispatch } from 'react-redux'
import {
	closeModal,
	openModal,
} from '../../redux/actionCreators/modalActionCreators'

const Dispatcher = () => {
	const dispatch = useDispatch()

	return (
		<Button
			text='open modal'
			className='test btn'
			backgroundColor='green'
			onClick={() => dispatch(openModal())}
		/>
	)
}

const Component = () => {
	const [isYes, setIsYes] = useState(false)
	const isModalOpen = useSelector(state => state.modal.isModalOpen)
	const dispatch = useDispatch()
	console.log(isModalOpen)

	const monkFuncYes = () => {
		setIsYes(true)
		dispatch(closeModal())
	}
	const monkFuncNo = () => {
		setIsYes(false)
		dispatch(closeModal())
	}

	return (
		<Provider store={store}>
			<Dispatcher />
			{isModalOpen && (
				<Modal
					header='test test'
					text='modalWindow'
					isCloseButton={true}
					onClick={() => dispatch(closeModal())}
					children={
						<div>
							<Button
								text='yes'
								className='test btn'
								backgroundColor='green'
								onClick={monkFuncYes}
							/>
							<Button
								text='no'
								className='test btn'
								backgroundColor='green'
								onClick={monkFuncNo}
							/>
						</div>
					}
				/>
			)}
			{isYes && <span>Yes</span>}
			{!isYes && <span>No</span>}
		</Provider>
	)
}

describe('Smoke test (snapshot tests)', () => {
	test('should modal render', () => {
		const { asFragment } = render(
			<Modal
				header='test test'
				text='test test'
				isCloseButton={true}
				onClick={() => {}}
				children={
					<div>
						<Button
							text='yes'
							className='test btn'
							backgroundColor='green'
							onClick={() => {}}
						/>
						<Button
							text='no'
							className='test btn'
							backgroundColor='green'
							onClick={() => {}}
						/>
					</div>
				}
			/>
		)
		expect(asFragment()).toMatchSnapshot()
	})
})

describe('Modal onClick tests', () => {
	test('should modal isOpen on click', () => {
		render(
			<Provider store={store}>
				<Component />
			</Provider>
		)
		const openModalBtn = screen.getByText('open modal')
		fireEvent.click(openModalBtn)
		const modal = screen.getByText('modalWindow')
		expect(modal).toBeInTheDocument()
	})
	test('should modal isCloseButton work', () => {
		render(
			<Provider store={store}>
				<Component />
			</Provider>
		)
		const openModalBtn = screen.getByText('open modal')
		fireEvent.click(openModalBtn)
		fireEvent.click(screen.getByText('X'))
		const modal = screen.queryByText('modalWindow')
		expect(modal).not.toBeInTheDocument()
	})
	test('should modal children YesBtn work', () => {
		render(
			<Provider store={store}>
				<Component />
			</Provider>
		)
		const openModalBtn = screen.getByText('open modal')
		fireEvent.click(openModalBtn)
		fireEvent.click(screen.getByText('yes'))
		expect(screen.getByText('Yes')).toBeInTheDocument()
		const modal = screen.queryByText('modalWindow')
		expect(modal).not.toBeInTheDocument()
	})
	test('should modal children NoBtn work', () => {
		render(
			<Provider store={store}>
				<Component />
			</Provider>
		)
		const openModalBtn = screen.getByText('open modal')
		fireEvent.click(openModalBtn)
		fireEvent.click(screen.getByText('no'))
		expect(screen.getByText('No')).toBeInTheDocument()
		const modal = screen.queryByText('modalWindow')
		expect(modal).not.toBeInTheDocument()
	})
})
