import React from 'react'
import styles from './Modal.module.scss'
import propTypes from 'prop-types'

const Modal = ({
	isCloseButton,
	headerBg = 'white',
	mainBg = 'white',
	header,
	text,
	children,
	onClick,
}) => {
	return (
		<div
			className={styles.modal}
			onClick={e => {
				if (e.target === e.currentTarget) {
					onClick()
				}
			}}
		>
			<div className={styles.modalContent} style={{ backgroundColor: mainBg }}>
				<div
					className={styles.modalHeader}
					style={{ backgroundColor: headerBg }}
				>
					<h2>{header}</h2>
					{isCloseButton && <button onClick={onClick}>X</button>}
				</div>
				<div className={styles.modalMain}>
					<p>{text}</p>
					<div className={styles.modalBtns}>{children}</div>
				</div>
			</div>
		</div>
	)
}

Modal.propTypes = {
	isCloseButton: propTypes.bool,
	headerBg: propTypes.string,
	mainBg: propTypes.string,
	header: propTypes.string,
	text: propTypes.string,
	children: propTypes.node,
	onClick: propTypes.func,
}

Modal.defaultProps = {
	isCloseButton: false,
	headerBg: 'white',
	mainBg: 'white',
	header: 'Modal Header',
	text: 'Modal Text',
	onClick: () => {},
}
export default Modal
