import React, { memo, useEffect } from 'react'
import styles from './ProductItem.module.scss'
import Icon from '@mdi/react'
import { mdiStarPlusOutline, mdiStar } from '@mdi/js'
import { useState } from 'react'
import Button from '../../Button'
import propTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { toggleFavItem } from '../../../redux/actionCreators/favoriteItemsActionCreators'
import { addItemsToCart } from '../../../redux/actionCreators/cartItemsActionCreators'
import {
	closeModal,
	openModal,
	setModalContent,
} from '../../../redux/actionCreators/modalActionCreators'

const ProductItem = props => {
	const {
		name,
		price,
		image,
		sku,
		color,
		index,
		nowIsInCart,
		nowIsFavorite,
	} = props

	const [isFavorite, setIsFavorite] = useState(false)
	const [isInCart, setIsInCart] = useState(false)
	const [isOkayButton, setIsOkayButton] = useState(false)

	const dispatch = useDispatch()

	useEffect(() => {
		if (nowIsInCart) {
			setIsInCart(nowIsInCart)
		}
		if (nowIsFavorite) {
			setIsFavorite(nowIsFavorite)
		}
	}, [])

	const handleIsOkayButton = bool => {
		setIsOkayButton(bool)
		dispatch(closeModal())
	}

	const handleModal = () => {
		dispatch(openModal())
		dispatch(
			setModalContent({
				header: 'Add To Cart',
				text: 'Do you want to add this item to your cart?',
				buttons: [
					<Button
						backgroundColor='green'
						text='Yes'
						onClick={() => handleIsOkayButton(true)}
						key='agreeButton'
					/>,
					<Button
						backgroundColor='red'
						text='No'
						onClick={() => handleIsOkayButton(false)}
						key='disagreeButton'
					/>,
				],
			})
		)
	}
	const addMoreToCart = () => {
		dispatch(addItemsToCart(index))
	}
	const handleAddToCart = () => {
		if (isOkayButton) {
			dispatch(addItemsToCart(index))
			setIsInCart(true)
		}
	}
	useEffect(() => {
		handleAddToCart()
	}, [isOkayButton])

	return (
		<div className={styles.itemConainer}>
			<h3>{name}</h3>
			<img src={image} alt={name} />
			<p>Price: {price}$</p>
			<p>Color: {color}</p>
			<Button
				backgroundColor={!isInCart ? 'gray' : 'green'}
				text='Add To Cart'
				onClick={!isInCart ? () => handleModal() : () => addMoreToCart()}
			/>
			<div className={styles.skuContainer}>
				<Icon
					onClick={() => {
						setIsFavorite(prev => !prev)
						dispatch(toggleFavItem(index))
					}}
					path={!isFavorite ? mdiStarPlusOutline : mdiStar}
					size={1}
					color={!isFavorite ? 'black' : 'green'}
				/>
				<p>{sku}</p>
			</div>
		</div>
	)
}

ProductItem.propTypes = {
	name: propTypes.string.isRequired,
	price: propTypes.number.isRequired,
	image: propTypes.string.isRequired,
	sku: propTypes.string.isRequired,
	color: propTypes.string,
	nowIsInCart: propTypes.bool,
	nowIsFavorite: propTypes.bool,
	index: propTypes.object.isRequired,
}

ProductItem.defaultProps = {
	color: 'No Data',
	nowIsFavorite: false,
	nowIsInCart: false,
}

export default memo(ProductItem)
