import React, { useContext, useEffect, useMemo } from 'react'
import ProductItem from './ProductItem'
import styles from './ProductContainer.module.scss'
import propTypes from 'prop-types'
import { useSelector } from 'react-redux'
import { shallowEqual } from 'react-redux'
import { ItemsContainerContext } from '../../contexts/ItemsContainerContext'

const ProductContainer = () => {
	const items = useSelector(state => state.items.items, shallowEqual)
	const favoriteItems = useSelector(
		state => state.favItems.favItems,
		shallowEqual
	)
	const isTableDisplay = useContext(ItemsContainerContext).isTableDisplay
	const setIsTableDisplay = useContext(ItemsContainerContext).setIsTableDisplay

	return (
		<div className={styles.root}>
			<div className={styles.btnSwitcherContainer}>
				<svg
					xmlns='http://www.w3.org/2000/svg'
					viewBox='0 0 20 20'
					fill='currentColor'
					className={styles.btnSwitcher}
					onClick={() => {
						setIsTableDisplay(true)
					}}
					style={{
						color: isTableDisplay ? 'green' : '#4840bd',
					}}
				>
					<path
						fillRule='evenodd'
						d='M.99 5.24A2.25 2.25 0 013.25 3h13.5A2.25 2.25 0 0119 5.25l.01 9.5A2.25 2.25 0 0116.76 17H3.26A2.267 2.267 0 011 14.74l-.01-9.5zm8.26 9.52v-.625a.75.75 0 00-.75-.75H3.25a.75.75 0 00-.75.75v.615c0 .414.336.75.75.75h5.373a.75.75 0 00.627-.74zm1.5 0a.75.75 0 00.627.74h5.373a.75.75 0 00.75-.75v-.615a.75.75 0 00-.75-.75H11.5a.75.75 0 00-.75.75v.625zm6.75-3.63v-.625a.75.75 0 00-.75-.75H11.5a.75.75 0 00-.75.75v.625c0 .414.336.75.75.75h5.25a.75.75 0 00.75-.75zm-8.25 0v-.625a.75.75 0 00-.75-.75H3.25a.75.75 0 00-.75.75v.625c0 .414.336.75.75.75H8.5a.75.75 0 00.75-.75zM17.5 7.5v-.625a.75.75 0 00-.75-.75H11.5a.75.75 0 00-.75.75V7.5c0 .414.336.75.75.75h5.25a.75.75 0 00.75-.75zm-8.25 0v-.625a.75.75 0 00-.75-.75H3.25a.75.75 0 00-.75.75V7.5c0 .414.336.75.75.75H8.5a.75.75 0 00.75-.75z'
						clipRule='evenodd'
					/>
				</svg>
				<svg
					xmlns='http://www.w3.org/2000/svg'
					viewBox='0 0 20 20'
					fill='currentColor'
					className={styles.btnSwitcher}
					onClick={() => {
						setIsTableDisplay(false)
					}}
					style={{
						color: isTableDisplay ? '#4840bd' : 'green',
					}}
				>
					<path d='M14 17h2.75A2.25 2.25 0 0019 14.75v-9.5A2.25 2.25 0 0016.75 3H14v14zM12.5 3h-5v14h5V3zM3.25 3H6v14H3.25A2.25 2.25 0 011 14.75v-9.5A2.25 2.25 0 013.25 3z' />
				</svg>
			</div>
			<div
				className={styles.productContainer}
				style={{
					flexDirection: isTableDisplay ? 'row' : 'column',
				}}
			>
				{items.map((item, index) => {
					const favIndex = favoriteItems.findIndex(el => el.sku === item.sku)

					if (favIndex !== -1) {
						if (favoriteItems[favIndex].nowIsInFavorite === false) {
							item.nowIsInFavorite = false
						} else {
							item.nowIsInFavorite = true
						}
					} else {
						item.nowIsInFavorite = false
					}

					return (
						<ProductItem
							key={item.sku}
							name={item.name}
							price={item.price}
							image={item.imageURL}
							sku={item.sku}
							color={item.color}
							count={item?.count}
							index={items[index]}
							nowIsInCart={item?.nowIsInCart}
							nowIsFavorite={item?.nowIsInFavorite}
						/>
					)
				})}
			</div>
		</div>
	)
}

ProductContainer.propTypes = {
	items: propTypes.array,
	addToFavorite: propTypes.func,
	setCartPrice: propTypes.func,
}

ProductContainer.defaultProps = {
	items: [],
	addToFavorite: () => {},
	setCartPrice: () => {},
}

export default ProductContainer
