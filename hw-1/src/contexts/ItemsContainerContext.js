import { createContext } from 'react'
import { useState } from 'react'

const ItemsContainerContext = createContext()

const ItemsContainerProvider = ({ children }) => {
	const [isTableDisplay, setIsTableDisplay] = useState(true)

	return (
		<ItemsContainerContext.Provider
			value={{ isTableDisplay, setIsTableDisplay }}
		>
			{children}
		</ItemsContainerContext.Provider>
	)
}

export { ItemsContainerContext, ItemsContainerProvider }
