import React, { useEffect } from 'react'
import styles from './CartPage.module.scss'
import propTypes from 'prop-types'
import { useState } from 'react'
import Icon from '@mdi/react'
import { mdiDelete } from '@mdi/js'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import Button from '../../components/Button'
import { removeFromCart } from '../../redux/actionCreators/cartItemsActionCreators'
import {
	closeModal,
	openModal,
	setModalContent,
} from '../../redux/actionCreators/modalActionCreators'
import CartForm from '../../components/Forms/CartForm'

const CartPage = ({ items, cartPrice }) => {
	const [isOkayButton, setIsOkayButton] = useState(false)
	const [currentItem, setcurrentItem] = useState(null)

	const dispatch = useDispatch()

	const handleIsOkayButton = bool => {
		setIsOkayButton(bool)
		dispatch(closeModal())
	}
	const handleModal = item => {
		setcurrentItem(item)
		dispatch(openModal())
		dispatch(
			setModalContent({
				header: 'Delete from Cart',
				text: 'Do you want to delete this item?',
				buttons: [
					<Button
						backgroundColor='green'
						text='Yes'
						onClick={() => handleIsOkayButton(true)}
						key='agreeButton'
					/>,
					<Button
						backgroundColor='red'
						text='No'
						onClick={() => handleIsOkayButton(false)}
						key='disagreeButton'
					/>,
				],
			})
		)
	}

	useEffect(() => {
		if (isOkayButton) {
			dispatch(removeFromCart(currentItem))
			setIsOkayButton(false)
		}
	}, [isOkayButton])
	return (
		<div className={styles.container}>
			<h1>Cart</h1>
			<div className={styles.itemsContainer}>
				{items.map(item => {
					return (
						<div className={styles.itemContainer} key={item.sku}>
							{' '}
							<Link to={`/cart/:${item.sku}`}>
								{' '}
								<img src={item.imageURL} alt='' />
							</Link>
							<h2>{item.name}</h2>
							<p>{item.price}</p>
							<p>{item.count}</p>
							<Icon
								onClick={() => handleModal(item)}
								path={mdiDelete}
								size={1.2}
							/>
						</div>
					)
				})}
			</div>
			{items.length === 0 ? (
				<h2>Your cart is empty</h2>
			) : (
				<h2 className={styles.cartPrice}>Total Price: {cartPrice}</h2>
			)}

			{items.length !== 0 && (
				<CartForm currentItem={items} cartPrice={cartPrice} />
			)}
		</div>
	)
}

CartPage.propTypes = {
	items: propTypes.array.isRequired,
	cartPrice: propTypes.oneOfType([propTypes.string, propTypes.number])
		.isRequired,
}

export default CartPage
