import React from 'react'
import styles from '../CartPage/CartPage.module.scss'
import Modal from '../../components/Modal'
import Button from '../../components/Button'
import Icon from '@mdi/react'
import { mdiStar } from '@mdi/js'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { toggleFavItem } from '../../redux/actionCreators/favoriteItemsActionCreators'
import {
	closeModal,
	openModal,
	setModalContent,
} from '../../redux/actionCreators/modalActionCreators'
import { shallowEqual } from 'react-redux'

const FavoritePage = () => {
	const [isOkayButton, setIsOkayButton] = useState(false)
	const [currentItem, setCurrentItem] = useState(null)
	const [onlyFavoriteItems, setOnlyFavoriteItems] = useState([])

	const dispatch = useDispatch()
	const favoriteItems = useSelector(
		state => state.favItems.favItems,
		shallowEqual
	)

	useEffect(() => {
		setOnlyFavoriteItems(
			favoriteItems.filter(item => item.nowIsInFavorite === true)
		)
	}, [favoriteItems])

	const handleIsOkayButton = bool => {
		setIsOkayButton(bool)
		dispatch(closeModal())
	}

	const handleModal = item => {
		setCurrentItem(item)
		dispatch(openModal())
		dispatch(
			setModalContent({
				header: 'Delete from Favorite',
				text: 'Do you want to delete this item?',
				buttons: [
					<Button
						backgroundColor='green'
						text='Yes'
						onClick={() => handleIsOkayButton(true)}
						key='agreeButton'
					/>,
					<Button
						backgroundColor='red'
						text='No'
						onClick={() => handleIsOkayButton(false)}
						key='disagreeButton'
					/>,
				],
			})
		)
	}

	useEffect(() => {
		if (isOkayButton) {
			dispatch(toggleFavItem(currentItem))
			setIsOkayButton(false)
		}
	}, [isOkayButton])

	return (
		<div className={styles.container}>
			<h1>Favorite</h1>
			<div className={styles.itemsContainer}>
				{onlyFavoriteItems.length > 0 ? (
					onlyFavoriteItems.map(item => (
						<div className={styles.itemContainer} key={item.sku}>
							<Link to={`/favorite/:${item.sku}`}>
								{' '}
								<img src={item.imageURL} alt='' />
							</Link>
							<h2>{item.name}</h2>
							<p>{item.price}</p>
							<p>{item.count}</p>
							<Icon
								onClick={() => handleModal(item)}
								path={mdiStar}
								color={'green'}
								size={1.2}
							/>
						</div>
					))
				) : (
					<h2>Your favorite is empty</h2>
				)}
			</div>
		</div>
	)
}

export default FavoritePage
