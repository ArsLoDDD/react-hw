import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router'
import { useNavigate } from 'react-router-dom'
import styles from './ItemPage.module.scss'
import propTypes from 'prop-types'

const SIMPLE_DESCRIPTION =
	'This product is known for its exceptional quality, delivering top-notch performance and reliability. With a sleek and modern design, it offers both style and functionality. Its advanced features and intuitive interface make it user-friendly and suitable for various applications. Customers praise its durability and long-lasting performance, making it a worthwhile investment. Whether for personal or professional use, this product consistently exceeds expectations and is highly recommended by satisfied users worldwide.'

const ItemPage = ({ items }) => {
	const location = useLocation()
	const navigate = useNavigate()

	const [item, setItem] = useState(null)

	useEffect(() => {
		const id = location.pathname.split(':').pop()
		const item = items.find(item => item.sku === id)

		setItem(item)
		if (!item && items.length > 0) {
			setTimeout(() => {
				console.log('redirect')
				navigate('/')
			}, 4000)
		}
	}, [item, items, location.pathname])

	return (
		<div>
			{!item && <h1>Loading...</h1>}
			{item && (
				<div className={styles.container}>
					<h2>{item.name}</h2>
					<img src={item.imageURL} alt={item.name} />
					<p className={styles.price}>Price: {item.price}</p>
					<p className={styles.description}>
						{item.description || SIMPLE_DESCRIPTION}
					</p>
				</div>
			)}
		</div>
	)
}

ItemPage.propTypes = {
	items: propTypes.array,
}

ItemPage.defaultProps = {
	items: [],
}

export default ItemPage
