import {
	SET_CART_COUNT,
	INCREASED_CART_COUNT,
	DECREASED_CART_COUNT,
} from '../actions/cartCountActions'

export const setCartCount = array => ({ type: SET_CART_COUNT, payload: array })

export const increasedCartCount = (value = 1) => ({
	type: INCREASED_CART_COUNT,
	payload: value,
})

export const decreasedCartCount = (value = 1) => ({
	type: DECREASED_CART_COUNT,
	payload: value,
})
