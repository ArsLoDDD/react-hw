import {
	ADD_CART_ITEM,
	REMOVE_CART_ITEM,
	SET_CART_ITEMS,
	RESET_CART_ITEMS,
} from '../actions/cartItemsActions'
import { increasedCartPrice, setCartPrice } from './cartPriceActionCreators'
import { decreasedCartPrice } from './cartPriceActionCreators'
import {
	decreasedCartCount,
	setCartCount,
	increasedCartCount,
} from './cartCountActionCreators'

export const addItemsToCart = item => {
	return dispatch => {
		dispatch({
			type: ADD_CART_ITEM,
			payload: item,
		})
		dispatch(increasedCartPrice(item.price))
		dispatch(increasedCartCount())
	}
}

export const setCartItems = items => ({
	type: SET_CART_ITEMS,
	payload: items,
})

export const removeFromCart = item => {
	return dispatch => {
		dispatch({
			type: REMOVE_CART_ITEM,
			payload: item,
		})
		dispatch(decreasedCartPrice(item.price))
		dispatch(decreasedCartCount())
	}
}

export const resetCartItems = () => ({
	type: RESET_CART_ITEMS,
})

export const fetchAndSetCartItems = () => {
	return async dispatch => {
		try {
			if (localStorage.getItem('cartItems')) {
				const items = JSON.parse(localStorage.getItem('cartItems'))
				await dispatch(setCartItems(items))
				dispatch(setCartPrice(items))
				dispatch(setCartCount(items))
			}
		} catch (error) {
			console.error(error)
		}
	}
}
