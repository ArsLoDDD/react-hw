import {
	SET_CART_PRICE,
	INCREASED_CART_PRICE,
	DECREASED_CART_PRICE,
} from '../actions/cartPriceActions'

export const setCartPrice = items => ({ type: SET_CART_PRICE, payload: items })

export const increasedCartPrice = price => ({
	type: INCREASED_CART_PRICE,
	payload: price,
})

export const decreasedCartPrice = price => ({
	type: DECREASED_CART_PRICE,
	payload: price,
})
