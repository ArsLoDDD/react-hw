import { SET_FAV_ITEMS, TOGGLE_FAV_ITEM } from '../actions/favoriteItemsActions'

export const toggleFavItem = sku => ({
	type: TOGGLE_FAV_ITEM,
	payload: sku,
})

export const setFavItems = items => ({
	type: SET_FAV_ITEMS,
	payload: items,
})
