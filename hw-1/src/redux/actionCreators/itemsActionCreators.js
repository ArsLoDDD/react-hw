import { SET_ITEMS } from '../actions/itemsActions'
// import axios from 'axios'

export const setItems = items => ({ type: SET_ITEMS, payload: items })

export const fetchItems = () => {
	return async dispatch => {
		try {
			// const { data } = await axios('/items.json')
			const response = await fetch('/items.json')
			const data = await response.json()
			dispatch(setItems(data))
		} catch (error) {
			console.error(error)
		}
	}
}
