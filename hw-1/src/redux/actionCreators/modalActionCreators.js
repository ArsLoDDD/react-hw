export const openModal = () => ({
	type: 'OPEN_MODAL',
})

export const closeModal = () => ({
	type: 'CLOSE_MODAL',
})

export const setModalContent = content => ({
	type: 'SET_MODAL_CONTENT',
	payload: content,
})
