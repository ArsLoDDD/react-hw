export const INCREASED_CART_COUNT = 'INCREASED_CART_COUNT'
export const DECREASED_CART_COUNT = 'DECREASED_CART_COUNT'
export const SET_CART_COUNT = 'SET_CART_COUNT'
