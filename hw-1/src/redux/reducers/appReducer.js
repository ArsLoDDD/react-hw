import { combineReducers } from 'redux'
import cartItemsReducer from './cartItemsReducer'
import favoriteItemsReducer from './favoriteItemsReducer'
import itemsReducer from './itemsReducer'
import cartPriceReducer from './cartPriceReducer'
import cartCountReducer from './cartCountReducer'
import modalReducer from './modalReducer'

const appReducer = combineReducers({
	cartItems: cartItemsReducer,
	favItems: favoriteItemsReducer,
	items: itemsReducer,
	cartPrice: cartPriceReducer,
	cartCount: cartCountReducer,
	modal: modalReducer,
})

export default appReducer
