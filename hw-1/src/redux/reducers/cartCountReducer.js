import {
	INCREASED_CART_COUNT,
	DECREASED_CART_COUNT,
	SET_CART_COUNT,
} from '../actions/cartCountActions'

export const initialValue = {
	cartCount: 0,
}

const cartCountReducer = (state = initialValue, action) => {
	switch (action.type) {
		case INCREASED_CART_COUNT:
			return { ...state, cartCount: Number(state.cartCount) + action.payload }
		case DECREASED_CART_COUNT:
			if (state.cartCount === 0) return state
			return { ...state, cartCount: Number(state.cartCount) - action.payload }
		case SET_CART_COUNT:
			if (action.payload !== 0) {
				const count = action.payload.reduce((acc, item) => {
					return acc + item.count
				}, 0)
				return { ...state, cartCount: count }
			} else {
				return { ...state, cartCount: 0 }
			}
		default:
			return state
	}
}

export default cartCountReducer
