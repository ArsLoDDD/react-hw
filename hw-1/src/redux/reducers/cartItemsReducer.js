import {
	ADD_CART_ITEM,
	REMOVE_CART_ITEM,
	SET_CART_ITEMS,
	RESET_CART_ITEMS,
} from '../actions/cartItemsActions'

const initialValue = {
	cartItems: [],
}

const cartItemsReducer = (state = initialValue, action) => {
	switch (action.type) {
		case ADD_CART_ITEM:
			let newArray = [...state.cartItems]
			const index = newArray.findIndex(el => el.sku === action.payload.sku)

			if (index !== -1) {
				newArray[index].count = newArray[index].count + 1
				localStorage.setItem('cartItems', JSON.stringify(newArray))

				return { ...state, cartItems: newArray }
			} else {
				const newItem = {
					...action.payload,
					count: 1,
					nowIsInCart: true,
				}
				newArray = [...newArray, newItem]
				localStorage.setItem('cartItems', JSON.stringify(newArray))
				return { ...state, cartItems: newArray }
			}

		case SET_CART_ITEMS:
			return { ...state, cartItems: action.payload }
		case RESET_CART_ITEMS:
			localStorage.removeItem('cartItems')
			return { ...state, cartItems: [] }
		case REMOVE_CART_ITEM:
			const newCartItems = [...state.cartItems]
			const indexRemoveItem = newCartItems.findIndex(
				item => item.sku === action.payload.sku
			)
			if (indexRemoveItem !== -1 && newCartItems[indexRemoveItem].count > 1) {
				newCartItems[indexRemoveItem].count =
					newCartItems[indexRemoveItem].count - 1
				localStorage.setItem('cartItems', JSON.stringify(newCartItems))
				return { ...state, cartItems: newCartItems }
			} else if (
				indexRemoveItem !== -1 &&
				newCartItems[indexRemoveItem].count === 1
			) {
				newCartItems.splice(indexRemoveItem, 1)
				localStorage.setItem('cartItems', JSON.stringify(newCartItems))
				return { ...state, cartItems: newCartItems }
			}
			return state
		default:
			return state
	}
}

export default cartItemsReducer
