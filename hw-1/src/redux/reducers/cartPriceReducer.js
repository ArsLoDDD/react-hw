import {
	INCREASED_CART_PRICE,
	DECREASED_CART_PRICE,
	SET_CART_PRICE,
} from '../actions/cartPriceActions'

const initialValue = {
	cartPrice: 0,
}

const cartPriceReducer = (state = initialValue, action) => {
	switch (action.type) {
		case INCREASED_CART_PRICE:
			const newIncPrice = Number(state.cartPrice) + Number(action.payload)
			return { ...state, cartPrice: newIncPrice.toFixed(2) }
		case DECREASED_CART_PRICE:
			if (Number(state.cartPrice) - Number(action.payload) < 0) return state
			const newDecPrice = Number(state.cartPrice) - Number(action.payload)
			return { ...state, cartPrice: newDecPrice.toFixed(2) }
		case SET_CART_PRICE:
			if (action.payload !== 0) {
				const newPrice = action.payload.reduce(
					(acc, item) => acc + Number(item.price) * Number(item.count),
					0
				)

				return { ...state, cartPrice: newPrice.toFixed(2) }
			} else {
				return { ...state, cartPrice: 0 }
			}
		default:
			return state
	}
}

export default cartPriceReducer
