import { SET_FAV_ITEMS, TOGGLE_FAV_ITEM } from '../actions/favoriteItemsActions'

const initialValue = {
	favItems: [],
}

const favoriteItemsReducer = (state = initialValue, action) => {
	switch (action.type) {
		case TOGGLE_FAV_ITEM:
			const newFavItems = [...state.favItems]

			const itemIndex = newFavItems.findIndex(
				item => item.sku === action.payload.sku
			)

			if (itemIndex !== -1) {
				newFavItems[itemIndex].nowIsInFavorite =
					!newFavItems[itemIndex].nowIsInFavorite
				localStorage.setItem('favoriteItems', JSON.stringify(newFavItems))
				return { ...state, favItems: newFavItems }
			} else if (itemIndex === -1) {
				newFavItems.push(action.payload)
				newFavItems[newFavItems.length - 1].nowIsInFavorite = true
				localStorage.setItem('favoriteItems', JSON.stringify(newFavItems))

				return { ...state, favItems: newFavItems }
			}

		case SET_FAV_ITEMS:
			return { ...state, favItems: action.payload }

		default:
			return state
	}
}

export default favoriteItemsReducer
