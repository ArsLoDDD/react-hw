import { SET_ITEMS } from '../actions/itemsActions'

const initialValue = {
	items: [],
}

const itemsReducer = (state = initialValue, action) => {
	switch (action.type) {
		case SET_ITEMS:
			return { ...state, items: action.payload }
		default:
			return state
	}
}

export default itemsReducer
