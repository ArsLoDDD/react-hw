import {
	OPEN_MODAL,
	CLOSE_MODAL,
	SET_MODAL_CONTENT,
} from '../actions/modalActions'

export const initialValue = {
	isModalOpen: false,
	modalContent: {
		header: '',
		text: '',
		buttons: [],
	},
}

const modalReducer = (state = initialValue, action) => {
	switch (action?.type) {
		case OPEN_MODAL:
			return { ...state, isModalOpen: true }
		case CLOSE_MODAL:
			return { ...state, isModalOpen: false }
		case SET_MODAL_CONTENT:
			return { ...state, modalContent: action.payload }
		default:
			return state
	}
}

export default modalReducer
