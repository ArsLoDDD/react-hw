import cartCountReducer from '../cartCountReducer'
import {
	setCartCount,
	increasedCartCount,
	decreasedCartCount,
} from '../../actionCreators/cartCountActionCreators'
import { initialValue } from '../cartCountReducer'

describe('cartCountReducer tests', () => {
	test('should set cartCount: 0', () => {
		expect(cartCountReducer(undefined, setCartCount(0))).toEqual({
			...initialValue,
			cartCount: 0,
		})
	})
	test('should increase cartCount without params', () => {
		expect(cartCountReducer(undefined, increasedCartCount())).toEqual({
			...initialValue,
			cartCount: 1,
		})
	})
	test('should increase cartCount with params', () => {
		expect(cartCountReducer(undefined, increasedCartCount(5))).toEqual({
			...initialValue,
			cartCount: 5,
		})
	})
	test('should decrease cartCount without params and test cartCount can`t be lower than 0', () => {
		expect(cartCountReducer(undefined, decreasedCartCount())).toEqual({
			...initialValue,
			cartCount: 0,
		})
	})
	test('should decrease cartCount with params and test cartCount can`t be lower than 0', () => {
		expect(cartCountReducer(undefined, decreasedCartCount(5))).toEqual({
			...initialValue,
			cartCount: 0,
		})
	})
	test('should decrease cartCount with params', () => {
		const state = { ...initialValue, cartCount: 5 }
		expect(cartCountReducer(state, decreasedCartCount(2))).toEqual({
			...initialValue,
			cartCount: 3,
		})
	})
	test('should decrease cartCount without params', () => {
		const state = { ...initialValue, cartCount: 5 }
		expect(cartCountReducer(state, decreasedCartCount())).toEqual({
			...initialValue,
			cartCount: 4,
		})
	})
})
