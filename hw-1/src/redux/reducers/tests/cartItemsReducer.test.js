import cartItemsReducer from '../cartItemsReducer'
import { initialValue } from '../cartItemsReducer'

import {
	setCartItems,
	resetCartItems,
} from '../../actionCreators/cartItemsActionCreators'

import { ADD_CART_ITEM } from '../../actions/cartItemsActions'

describe('cartItemsReducer tests', () => {
	test('should setCartItems set params items', () => {
		expect(cartItemsReducer(undefined, setCartItems([1, 2, 3]))).toEqual({
			...initialValue,
			cartItems: [1, 2, 3],
		})
	})
	test('should resetCartItems reset all items from state', () => {
		const state = {
			...initialValue,
			cartItems: [1, 2, 3],
		}
		expect(cartItemsReducer(state, resetCartItems())).toEqual({
			...initialValue,
			cartItems: [],
		})
	})
	test('should add item to cart', () => {
		expect(
			cartItemsReducer(undefined, {
				type: ADD_CART_ITEM,
				payload: {
					sku: 1,
					price: 100,
					name: 'test',
				},
			})
		).toEqual({
			...initialValue,
			cartItems: [
				{
					sku: 1,
					price: 100,
					name: 'test',
					count: 1,
					nowIsInCart: true,
				},
			],
		})
	})
})
