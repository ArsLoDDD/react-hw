import cartPriceReducer from '../cartPriceReducer'
import { initialValue } from '../cartPriceReducer'
import {
	setCartPrice,
	increasedCartPrice,
	decreasedCartPrice,
} from '../../actionCreators/cartPriceActionCreators'

describe('cartPriceReducer tests', () => {
	test('should setCartPrice set params value', () => {
		expect(
			cartPriceReducer(
				undefined,
				setCartPrice([
					{
						price: 100,
						count: 1,
					},
					{
						price: 100,
						count: 1,
					},
				])
			)
		).toEqual({
			...initialValue,
			cartPrice: '200.00',
		})
	})
	test('should increasedCartPrice increase price with params', () => {
		expect(cartPriceReducer(undefined, increasedCartPrice(100))).toEqual({
			...initialValue,
			cartPrice: '100.00',
		})
	})
	test('should decreasedCartPrice decrease price with params', () => {
		const state = {
			...initialValue,
			cartPrice: 100,
		}
		expect(cartPriceReducer(state, decreasedCartPrice(50))).toEqual({
			...initialValue,
			cartPrice: '50.00',
		})
	})
	test('should decreasedCartPrice decrease price with params but cant be less than 0', () => {
		expect(cartPriceReducer(undefined, decreasedCartPrice(100))).toEqual({
			...initialValue,
			cartPrice: 0,
		})
	})
})
