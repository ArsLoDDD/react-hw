import favoriteItemsReducer from '../favoriteItemsReducer'
import { initialValue } from '../favoriteItemsReducer'
import {
	toggleFavItem,
	setFavItems,
} from '../../actionCreators/favoriteItemsActionCreators'

describe('favoriteItemsReducer tests', () => {
	test('should setFavItems set new items to fav list', () => {
		expect(
			favoriteItemsReducer(
				undefined,
				setFavItems([
					{
						sku: 1,
						nowIsInFavorite: false,
					},
					{
						sku: 2,
						nowIsInFavorite: false,
					},
				])
			)
		).toEqual({
			favItems: [
				{
					sku: 1,
					nowIsInFavorite: false,
				},
				{
					sku: 2,
					nowIsInFavorite: false,
				},
			],
		})
	})
	test('should toggleFavItem toggle item (false) in fav list', () => {
		expect(
			favoriteItemsReducer(
				undefined,
				toggleFavItem({
					sku: 1,
					nowIsInFavorite: false,
				})
			)
		).toEqual({
			favItems: [
				{
					sku: 1,
					nowIsInFavorite: true,
				},
			],
		})
	})
	test('should toggleFavItem toggle item (true) in fav list', () => {
		expect(
			favoriteItemsReducer(
				{
					favItems: [
						{
							sku: 1,
							nowIsInFavorite: true,
						},
					],
				},
				toggleFavItem({
					sku: 1,
					nowIsInFavorite: true,
				})
			)
		).toEqual({
			favItems: [
				{
					sku: 1,
					nowIsInFavorite: false,
				},
			],
		})
	})
})
