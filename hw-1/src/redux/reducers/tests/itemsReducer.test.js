import itemsReducer from '../itemsReducer'
import { initialValue } from '../itemsReducer'
import { setItems } from '../../actionCreators/itemsActionCreators'

describe('itemsReducer tests', () => {
	test('should setItems set items on the state', () => {
		expect(itemsReducer(undefined, setItems([{ sku: 1 }, { sku: 2 }]))).toEqual(
			{
				...initialValue,
				items: [
					{
						sku: 1,
					},
					{
						sku: 2,
					},
				],
			}
		)
	})
})
