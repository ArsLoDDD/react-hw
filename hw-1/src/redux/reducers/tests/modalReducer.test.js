import modalReducer from '../modalReducer'
import { initialValue } from '../modalReducer'
import {
	openModal,
	closeModal,
	setModalContent,
} from '../../actionCreators/modalActionCreators'

describe('modalReducer tests', () => {
	test('modal shold return defaul value (state)', () => {
		expect(modalReducer(undefined, {})).toEqual(initialValue)
	})
	test('should modal is open', () => {
		expect(modalReducer(undefined, openModal())).toEqual({
			...initialValue,
			isModalOpen: true,
		})
	})
	test('should modal is close', () => {
		expect(modalReducer(undefined, closeModal())).toEqual({
			...initialValue,
			isModalOpen: false,
		})
	})
	test('should modal is set content', () => {
		expect(
			modalReducer(
				undefined,
				setModalContent({
					header: 'test',
					text: 'test',
					buttons: [],
				})
			)
		).toEqual({
			...initialValue,
			modalContent: {
				header: 'test',
				text: 'test',
				buttons: [],
			},
		})
	})
})
