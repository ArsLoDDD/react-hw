import { createStore } from 'redux'
import appReducer from './reducers/appReducer'
import { composeWithDevTools } from 'redux-devtools-extension'
import { applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

const store = createStore(
	appReducer,
	composeWithDevTools(applyMiddleware(thunk))
)
export default store
